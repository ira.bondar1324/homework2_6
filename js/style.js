"use strict";

function creatNewUser(){

    let name = prompt("Введіть ім'я нового користувача");
    let lastName = prompt("Введіть прізвище нового користувача");
    let dateOfBirth = prompt("Введіть дату народження у форматі дд.мм.рррр");
    const newUser = {};
    newUser._name = name;
    newUser._lastName=lastName;
    newUser.birthday = dateOfBirth;
    newUser.getAge = function(){
        let arrayBirth = newUser.birthday.split(".");
        const birthDate = new Date(`${arrayBirth[2]}-${arrayBirth[1]}-${arrayBirth[0]}`)
        let difference = Date.now() - birthDate.getTime();
        let result = parseInt(difference/1000/3600/24.013/365);
        return result;
    }
    newUser.getPassword = function(){
        let arrayBirth = newUser.birthday.split(".");
        let namePasword = (this._name.substring(0,1)).toUpperCase();
        let lastNamePasword = (this._lastName).toLowerCase();
        let password = `${namePasword}${lastNamePasword}${arrayBirth[2]}`;
        return password;
    }
    newUser.getLogin=function(){
        let nameAndLastName = (this._name.substring(0,1)+this._lastName).toLowerCase();
        return nameAndLastName;
    }
    Object.defineProperty(newUser, "_name" ,{
        writable: false
    });
    Object.defineProperty(newUser, "_lastName" ,{
        writable: false
    });
    newUser["set FirstName"]=function(value){
        Object.defineProperty(newUser, "_name" ,{
            writable: true
        });
        this._name = value;
    }
    newUser["set LastName"]=function(value){
        Object.defineProperty(newUser, "_lastName" ,{
            writable: true
        });
        this._lastName = value;
    }
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
}
creatNewUser();

